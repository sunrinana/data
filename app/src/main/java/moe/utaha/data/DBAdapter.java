package moe.utaha.data;

import android.content.Context;
import android.database.Cursor;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by chojeaho on 2016-08-10.
 */
public class DBAdapter extends CursorAdapter{

    public DBAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.custom_item,parent,false);

        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView id = (TextView) view.findViewById(R.id.text_id);
        TextView value = (TextView) view.findViewById(R.id.text_value);

        id.setText("ID : "+cursor.getString(cursor.getColumnIndex("id")));
        value.setText("VALUE : "+cursor.getString(cursor.getColumnIndex("value")));
    }
}
