package moe.utaha.data;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    ListView list;
    DBManager dbManager;
    SQLiteDatabase db;
    String query;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (ListView) findViewById(R.id.list_item);
        dbManager = new DBManager(getApplicationContext(),"test.db",null,1);

        Button btn_data = (Button) findViewById(R.id.btn_data);
        btn_data.setOnClickListener(this);

        SyncDB();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_data:
                Log.e("intent button","---------------onclick");
                Intent intent = new Intent(MainActivity.this,AddData.class);
                startActivity(intent);
                break;
        }
    }

    public void SyncDB()
    {
        db = dbManager.getWritableDatabase();
        query = "SELECT * FROM TEST;";

        cursor = db.rawQuery(query,null);
        if (cursor.getCount() > 0)
        {
            startManagingCursor(cursor);
            DBAdapter dbAdapter = new DBAdapter(this, cursor);
            list.setAdapter(dbAdapter);
        }
    }
}
