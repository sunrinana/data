package moe.utaha.data;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddData extends AppCompatActivity implements View.OnClickListener{

    EditText edit_id;
    EditText edit_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adddata);


        edit_id = (EditText) findViewById(R.id.edit_id);
        edit_text = (EditText) findViewById(R.id.edit_text);

        Button btn_add = (Button) findViewById(R.id.btn_add);
        Button btn_delete = (Button) findViewById(R.id.btn_delete);
        Button btn_update = (Button) findViewById(R.id.btn_update);
        btn_add.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
        btn_update.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        final DBManager dbManager = new DBManager(getApplicationContext(),"test.db",null,1);
        String id = edit_id.getText().toString();
        String value = edit_text.getText().toString();

        Intent intent = new Intent(AddData.this,MainActivity.class);
        switch (v.getId())
        {
            case R.id.btn_add:
                dbManager.insert("insert into TEST values (null ,"+id+",'"+value+"');");
                Toast.makeText(getApplicationContext(),"insert",Toast.LENGTH_SHORT).show();
                startActivity(intent);
                break;
            case R.id.btn_update:
                dbManager.update("update TEST set value = '" + value+ "' where id = " + id +";");
                Toast.makeText(getApplicationContext(),"update",Toast.LENGTH_SHORT).show();
                startActivity(intent);
                break;
            case R.id.btn_delete:
                dbManager.delete("delete from TEST where value ='"+value+"';");
                Toast.makeText(getApplicationContext(),"delete",Toast.LENGTH_SHORT).show();
                startActivity(intent);
                break;
        }
    }
}
